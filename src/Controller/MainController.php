<?php

namespace App\Controller;

use App\Entity\Answer;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function main(): Response
    {
        return $this->render('main/index.html.twig');
    }

    #[Route('/api/insert', name: 'api_insert')]
    public function api_insert(Request $request, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $answer = new Answer();  
        $answer->setContent($data->content);
        $answer->setQuestion($data->question);

        $em->persist($answer);
        $em->flush();

        return $this->json([
            'status' => 'OK',
            'message' => 'Your response was submitted',
        ]);
    }
}
