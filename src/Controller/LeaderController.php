<?php

namespace App\Controller;

use App\Repository\AnswerRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LeaderController extends AbstractController
{
    #[Route('/host', name: 'host')]
    public function leader(): Response
    {
        return $this->render('leader/index.html.twig');
    }

    #[Route('/api/data/{q}', name: 'api_data')]
    public function api_data(AnswerRepository $ar, int $q): Response
    {
        $data = $ar->findByQuestion($q);

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($data, 'json');
        return new JsonResponse($json);
    }

    #[Route('/api/purge', name: 'api_purge')]
    public function api_insert(AnswerRepository $ar, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $answers = $ar->findAll();
        foreach($answers as $answer) {
          $em->remove($answer);
        }
        $em->flush();

        return $this->json([
            'status' => 'OK',
            'message' => 'All entries were purged',
        ]);
    }
}
