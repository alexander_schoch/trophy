import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
	Box,
	Button,
  Container,
	Grid,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ClearIcon from '@mui/icons-material/Clear';

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Tooltip,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Tooltip,
);

class Leader extends React.Component {
  constructor(props) {
    super(props);

		this.state = {
			labels: [],
			data: [],
			question: 0,
			size: 0,
			show: true,
		};

		this.getData = this.getData.bind(this);
		this.makeDataNice = this.makeDataNice.bind(this);

		this.options = {
			indexAxis: 'y',
			elements: {
				bar: {
					borderWidth: 2,
				},
			},
			responsive: true,
			plugins: {
				title: {
					display: false,
				},
			},
			scales: {
				y: {
					ticks: {
						font: {
							size: 20
						}
					}
				}
			}
		};
  }

	componentDidMount() {
		this.getData();
		setInterval(this.getData, 2000);
	}

  async getData() {
    const response = await fetch('/api/data/' + this.state.question);
    const data = await response.json();
		this.makeDataNice(data);
  }

	makeDataNice(data) {
		let strings = [];
		let occurences = [];
		data.map((entry) => {
			let lowercasecontent = entry.content.toLowerCase();
			let content = lowercasecontent.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

			let index = strings.indexOf(content);
			if(index < 0) {
				strings.push(content);
				occurences.push(1);
			} else {
				occurences[index] += 1;
			}
		});
    this.setState({
      labels: strings,
			data: occurences,
			size: data.length,
    });
	}


  render() {
		let labels = this.state.labels;
		let data = {
			labels,
			datasets: [
				{
					label: 'Dataset 1',
					data: this.state.data,
					borderColor: '#8C489F',
					backgroundColor: '#C3C3E5',
				},
			],
		};
    return (
      <ThemeProvider theme={theme}>
				<Box style={{ backgroundColor: theme.palette.primary.dark, paddingTop: '50px', paddingBottom: '50px' }}>
					<Container>
						{this.state.question == 0 
							? <Typography variant="h2" style={{ color: 'white' }}>Which Trophy?</Typography>
							: <Typography variant="h2" style={{ color: 'white' }}>Question {this.state.question}</Typography>}
						<Typography variant="h4" style={{ color: 'white' }}>{this.state.size} {this.state.size == 1 ? "Answer" : "Answers"}</Typography>
					</Container>
				</Box>
				<Container style={{ marginTop: '20px' }}>
					<Grid container spacing={2} style={{ marginBottom: '20px' }}>
						<Grid item xs={6}>
							<Button
								variant="contained"
								onClick={() => {this.setState({question: this.state.question - 1, show: true})}}
								disabled={this.state.question < 1}
								startIcon={<ArrowBackIcon/>}
							>
								Previous Question
							</Button>
						</Grid>
						<Grid item xs={6} style={{ textAlign: 'right' }}>
							<Button
								variant="contained"
								onClick={() => {this.setState({question: this.state.question + 1, show: false})}}
								disabled={this.state.question > 9}
								endIcon={<ArrowForwardIcon/>}
							>
								Next Question
							</Button>
						</Grid>
						<Grid item xs={6}>
							<Button
								variant="contained"
								onClick={() => {this.setState({show: !this.state.show})}}
							>
								Toggle Show
							</Button>
						</Grid>
						<Grid item xs={6} style={{ textAlign: 'right' }}>
							<Button
								variant="contained"
								onClick={() => {axios.post('/api/purge')}}
								disabled={this.state.question < 10}
								endIcon={<ClearIcon/>}
							>
								Purge Database
							</Button>
						</Grid>
					</Grid>
					{this.state.show && <Bar options={this.options} data={data} />}
				</Container>
			</ThemeProvider>
    );
  }
}

const container = document.getElementById('leader');
const root = ReactDOM.createRoot(container);
root.render(<Leader/>);
