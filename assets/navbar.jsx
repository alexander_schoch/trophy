import React from 'react';
import ReactDOM from 'react-dom/client';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Drawer from '@mui/material/Drawer';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Navbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawer: false,
    };

  }

  render () {
    return (
      <Box>
      <ThemeProvider theme={theme}>
      <AppBar position="sticky" style={{ backgroundColor: theme.palette.primary.dark }} elevation={0}>
        <Container>
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
           	</Box>
            { 
              // Desktop 
            }
            <a href="/">
              <img src="logo.svg" style={{ maxWidth: '180px' }}/>
            </a>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, marginLeft: '30px' }}>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      </ThemeProvider>
      </Box>
    );
  }
}

const container = document.getElementById('navbar');
const root = ReactDOM.createRoot(container);
root.render(<Navbar/>);
