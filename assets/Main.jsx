import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
	Box,
	Button,
  Container,
	FormControl,
	FormControlLabel,
	FormLabel,
	Grid,
	Radio,
	RadioGroup,
	TextField,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Main extends React.Component {
  constructor(props) {
    super(props);

		let content = [];
		let submitted = [];

		for(let i = 0; i < 11; i++) {
			content.push('');
			submitted.push(false);
		}

		this.state = {
			content: content,
			submitted: submitted,
		};

		this.handleSubmit = this.handleSubmit.bind(this);
  }

	handleSubmit(e) {
		let q = parseInt(e.target.value);
		if(this.state.content[q] != '') {
			axios.post('/api/insert', {content: this.state.content[q], question: q}).then(
				(response) => {
					if(response.data.status == 'OK') {
						let a = this.state.submitted;
						a[q] = true;
						this.setState({submitted: a});
					}
				}
			);
		}
	}

  render() {
		const numQuestions = 10;
		const trophies = [
			'Animals',
			'Cities',
			'Logos 1',
			'Logos 2',
		];
    return (
      <ThemeProvider theme={theme}>
				<Box style={{ backgroundColor: theme.palette.primary.dark, paddingTop: '50px', paddingBottom: '50px' }}>
					<Container>
						<Typography variant="h2" style={{ color: 'white' }}>PVK Trophy!</Typography>
					</Container>
				</Box>
				<Container>
						<Box style={{ marginTop: '20px', marginBottom: '50px' }}>
							<form>
								<Grid container spacing={2} style={{ marginTop: '1px' }}>
									<Grid item xs={12}>
										<FormControl>
											<FormLabel id="demo-radio-buttons-group-label" disabled={this.state.submitted[0]}>Which Trophy do you wanna play?</FormLabel>
											<RadioGroup
												aria-labelledby="demo-radio-buttons-group-label"
												name="radio-buttons-group"
												value={this.state.content[0]}
												onChange={(e) => {let a = this.state.content; a[0] = e.target.value; this.setState({content: a})}}
											>
												{trophies.map(trophy => (
													<FormControlLabel key={trophy} value={trophy} control={<Radio disabled={this.state.submitted[0]}/>} label={trophy} />
												
												))}
											</RadioGroup>
										</FormControl>
									</Grid>
									<Grid item xs={12}>
										<Button
											variant="contained"
											value="0"
											onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}
											disabled={this.state.submitted[0]}
											type="submit"
										>
											Submit
										</Button>
									</Grid>
								</Grid>
							</form>
						</Box>
							
						{
							[...Array(numQuestions)].map((i, v) => (
								<Box key={v} style={{ marginTop: '20px' }}>
									<form>
										<Grid container spacing={2} style={{ marginTop: '1px' }}>
											<Grid item xs={12}>
												<TextField
													value={this.state.content[v+1]}
													onChange={(e) => {let a = this.state.content; a[v+1] = e.target.value; this.setState({content: a})}}
													label={"Question " + (v + 1)}
													fullWidth
													disabled={this.state.submitted[v+1]}
												/>
											</Grid>
											<Grid item xs={12}>
												<Button
													variant="contained"
													value={v+1}
													onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}
													disabled={this.state.submitted[v+1]}
													type="submit"
												>
													Submit
												</Button>
											</Grid>
										</Grid>
									</form>
								</Box>
							)
						)}
				</Container>
			</ThemeProvider>
    );
  }
}

const container = document.getElementById('main');
const root = ReactDOM.createRoot(container);
root.render(<Main/>);
