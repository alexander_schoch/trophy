import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
	text: {
		secondary: '#888888',
	},
  palette: {
    type: 'light',
    primary: {
      main: '#8C489F',
      light: '#4caf50',
      contrastText: '#ffffff',
      dark: '#443266',
    },
    secondary: {
      main: '#b71c1c',
      light: '#f7c870',
      dark: '#560c0c',
    },
  },
});

export default theme;
